This module allow to create the media block: image, embedvideo, slideshow
easier by administrator.

DEPENDENCIES:
- File (Drupal 7.x Core)

INSTALLATION:
1. Download and extract the ResponsiveSlides.js library into the libraries
directory (usually "sites/all/libraries").
   Link: https://github.com/viljamis/ResponsiveSlides.js/archive/master.zip
   It will become: "sites/all/libraries/ResponsiveSlides.js/"

2. Download the module and simply copy it into your contributed modules folder:
[for example, your_drupal_path/sites/all/modules] and enable it from
the modules administration/management page.
More information at: Installing contributed modules (Drupal 7)

USAGE:
Go to the block management page (admin/structure/block) and click
"Add a media block" on the action link.
