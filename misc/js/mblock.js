/**
 * @file
 * Handles event action for site.
 */

(function ($) {

  Drupal.behaviors.mediaBlock = {
    attach: function (context) {
      $('.embedvideo').bind('click', function(e) {
        e.preventDefault();
        var delta = $(this).data('delta'),
        $parent = $(this).parent(),
        embedHTML = Drupal.settings.mediaBlock.embedvideo[delta];

        $parent.html(embedHTML);
      });

      // Enable slideshow.
      $(".rslides").each(function() {
        var delta = $(this).data('delta'),
        options = Drupal.settings.mediaBlock.slideshow[delta];

        $(this).responsiveSlides(options);
      });
    }
  };

})(jQuery);
