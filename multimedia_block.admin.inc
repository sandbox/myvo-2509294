<?php

/**
 * @file
 * Process admin page for to manage the multimedia block.
 */

/**
 * Menu callback: create a multimedia block.
 */
function multimedia_block_admin_create_ablock() {
  $form  = array();

  if (!multimedia_block_get_library_js()) {
    $download_url = 'https://github.com/viljamis/ResponsiveSlides.js';
    $args = array(
      '!url' => l($download_url, $download_url),
    );
    $msg = t('Please download the ResponsiveSlides library from !url.', $args);
    drupal_set_message($msg, 'warning');
  }

  $types = multimedia_block_get_type();
  $form['type'] = array(
    '#type'           => 'select',
    '#title'          => t('Block type'),
    '#required'       => TRUE,
    '#options'        => $types,
  );
  $form['title'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Block title (display on admin page)'),
    '#default_value'  => '[Mblock] ',
    '#required'       => TRUE,
    '#maxlength'      => 63,
  );
  $form['machine_name'] = array(
    '#type'           => 'machine_name',
    '#default_value'  => '',
    '#maxlength'      => 19,
    '#machine_name'   => array(
      'exists'        => 'multimedia_block_admin_is_exist_blockname',
    ),
  );

  $form['submit'] = array(
    '#type'       => 'submit',
    '#value'      => t('Continue to config block'),
  );
  $form['cancel'] = array(
    '#type'       => 'markup',
    '#markup'     => l(t('Cancel'), 'admin/structure/block'),
  );

  return $form;
}

/**
 * Is block name exist.
 *
 * We can not check exist at here because we can not get full block delta name,
 * so we always return FALSE.
 */
function multimedia_block_admin_is_exist_blockname($name) {
  return FALSE;
}

/**
 * Form validate: create a multimedia block.
 */
function multimedia_block_admin_create_ablock_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $block_delta = $values['type'] . '_' . $values['machine_name'];

  // Is block name exist.
  $query = 'SELECT * FROM {block} WHERE module = :module AND delta = :delta';
  $block = db_query($query, array(':module' => 'multimedia_block', ':delta' => $block_delta))->fetchObject();
  if (!empty($block)) {
    form_set_error('machine_name', t('The machine-readable name is already in use. It must be unique.'));
  }
}

/**
 * Form submit: create a multimedia block.
 */
function multimedia_block_admin_create_ablock_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  $block_delta = $values['type'] . '_' . $values['machine_name'];

  // Store new block.
  $blocknames = variable_get(MULTIMEDIA_BLOCK_NAMES_VAR, array());
  $blocknames[$block_delta] = array(
    'info'  => $values['title'],
    'cache' => DRUPAL_NO_CACHE,
  );
  variable_set(MULTIMEDIA_BLOCK_NAMES_VAR, $blocknames);

  // Clear cache block.
  block_flush_caches();
  $form_state['redirect'] = "admin/structure/block/manage/multimedia_block/{$block_delta}/configure";
}

/**
 * Menu callback: Delete a multimedia block.
 */
function multimedia_block_admin_delete_ablock($form, &$form_state, $mblock) {
  $form['info'] = array(
    '#type' => 'hidden',
    '#value' => $mblock['info'],
  );
  $form['bid'] = array(
    '#type' => 'hidden',
    '#value' => $mblock['delta'],
  );

  return confirm_form($form, t('Are you sure you want to delete the block %name?', array('%name' => $mblock['info'])), 'admin/structure/block', '', t('Delete'), t('Cancel'));
}

/**
 * Form submit: Delete a multimedia block.
 */
function multimedia_block_admin_delete_ablock_submit($form, &$form_state) {
  db_delete('block_custom')
    ->condition('bid', $form_state['values']['bid'])
    ->execute();
  db_delete('block')
    ->condition('module', 'multimedia_block')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  db_delete('block_role')
    ->condition('module', 'multimedia_block')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  drupal_set_message(t('The block %name has been removed.', array('%name' => $form_state['values']['info'])));

  $blocknames = variable_get(MULTIMEDIA_BLOCK_NAMES_VAR, array());
  unset($blocknames[$form_state['values']['bid']]);
  variable_set(MULTIMEDIA_BLOCK_NAMES_VAR, $blocknames);

  // Clear cache block.
  block_flush_caches();
  $form_state['redirect'] = 'admin/structure/block';
}
