<?php

/**
 * @file
 * This template handles the layout of the embedvideo block.
 *
 * Variables available:
 * - $delta: The delta key.
 * - $hover_img: The hover image to say "Click to play video".
 * - $imgurl: The cover image of the embedvideo.
 */
?>
<a class="embedvideo" data-delta="<?php echo $delta; ?>" href="#" title="<?php echo t('Click to play video'); ?>">
  <img src="<?php echo $hover_img; ?>" class="img-hover" />
  <img src="<?php echo $imgurl; ?>" class="img-cover" />
</a>
