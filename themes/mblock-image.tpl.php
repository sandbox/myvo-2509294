<?php

/**
 * @file
 * This template handles the layout of the image block.
 *
 * Variables available:
 * - $url: The link URL in the image block.
 * - $imgurl: The image file URL.
 */
?>
<?php if ($url): ?>
<a href="<?php echo $url; ?>">
  <img src="<?php echo $imgurl; ?>" />
</a>

<?php else: ?>
  <img src="<?php echo $imgurl; ?>" />
<?php endif; ?>
