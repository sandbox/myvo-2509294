<?php

/**
 * @file
 * This template handles the layout of the slideshow block.
 *
 * Variables available:
 * - $delta: The delta key.
 * - $slides: Array of slide info, the element of each slide:
 * [url, imgurl, title].
 */
?>
<ul class="rslides" data-delta="<?php echo $delta; ?>">
  <?php foreach ($slides as $slide): ?>
    <li>
      <?php if ($slide['url']): ?>
        <a href="<?php echo $slide['url']; ?>">
          <img src="<?php echo $slide['imgurl']; ?>" >
        </a>
      <?php else: ?>
        <img src="<?php echo $slide['imgurl']; ?>" >
      <?php endif; ?>

      <?php if ($slide['title']): ?>
        <p class="caption"><?php echo $slide['title']; ?></p>
      <?php endif; ?>
    </li>
  <?php endforeach; ?>
</ul>
